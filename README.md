# E-commerce demo

## Description

This is a example of a solution for a checkout operation of a e-commerce solution with a microservice approach.

The project was made using Spring Boot to build the microservices, for the sake of simplicity there are a parent project named ecommerce and three child projects:

- `checkout`: The REST API for checkout.
- `billing`: The service responsible for calculating the total cost of the bill.
- `logistic`: The service responsible for sending the products to the customer.

![components](/img/components.jpeg "Components")

We have to take into accout that there is no CI/CD configuration for this project, depending on how CI/CD will work, maybe the ecommerce project should be splited in three independen projects. Another aspect to see is that Checkout, Billing and Logistic services can be developed by different teams, so, it would be a good idea to have them separated, for now, to be able running the solution with a few steps the project is built as a Maven multi-module project.

### Checkout 

The Checkout project is the REST API for create an Order, it exposes an endpoint to send the Order as a Json object like folowing

```
{
    "clientId": 905,
    "date": "2020-07-05T06:45:17.130Z",
    "address": "9487 Boyle Light",
    "products": [
        {
            "id": 24,
            "quantity": 607,
            "cost": 478.67
        },
        {
            "id": 890,
            "quantity": 678,
            "cost": 355.37
        },
        {
            "id": 624,
            "quantity": 116,
            "cost": 772.51
        }
    ]
}
```

When the Order is received an identifier is generated for the order, then two messages are created to be sent to RabbitMQ whose destinations are the Billing and Logistic services.

### Billing

Billing service reads messages from RabbitMQ with the information about products, its quanties and costs, to perform the calculation of the total cost of the Order. 

At the moment the total cost of the order is logged. The plan for a future iteration of development is to have a database to store this, also having the persistence a tracking can be done, for example, to know if the bill was successfully created or not.

### Logistic 

Logistic service also reads messages from RabbitMQ with the information about products and the customer address, using this information the logistic to send the pruducts of the Order to the customer can be started. The plan for a future iteration is also have persistence to track the state of the delivery.

#### The model

There are two object that are used in this solution:

- Order
- Product

![Class Diagram](/img/classdiagram.jpg "Class Diagram")

To keep the model simple, both objects are used in all services, but they have slight differences to note that models in each service must be independent.

The address field was removed from the Order object in the Billing Service, on the other hand the cost field was removed from the Product object in the Logistic service.

## Frameworks, libraries and tools

### Spring Boot

Spring Boot is used a main framework because is a well-known framework that has evolved over the years and now is one of the most used frameworks to build microservices based solutions. It allows to create REST APIs easily, it has utilities to create docker images, it also have a good opinionated way of auto-configuring the applications.

### RabbitMQ

One of the microservices patterns is asychronous communication, to achieve this, messaging ammong services is used in this ecommerce solution. 

With RabbitMQ we can send and receive messages in an asynchronous way, if one of the services, Billing or Logistic are down, we sill are able to receive Orders, sending messages to RabbitMQ. However we have to pay a lot of attention in this component because it becomes in a point of failure, if the RabbitMQ is down the entire solution would be down, we want to ensure the High Availability for this component and enable the persistence of the messages.

### Maven

Maven is de facto management tool for Java projects, that's why is used in this solution.

Maven is the most used java project management tool, even with the existence of Gradle. This is maybe because to use Gradle have a learning curve due to its DSL language instead of Maven that uses XML, this is more verbose but everyone can start using it faster.

### Docker and Docker compose

Docker is used to packaged the applications involved in this ecommerce solution, it is a good fit to run microservices because isolates the execution of each application. In this case Docker Compose is used because it has an easy way to share resources among the Docker containers like the network, they can see them each other.

## Prerequisites

- JDK 1.8
- Docker
- Docker compose

## Build the docker images

In the parent project folder `ecommerce` execute the following command:

```
./mvnw spring-boot:build-image
```

This will create 3 docker images:

- checkout:0.1.0
- billing:0.1.0
- logistic:0.1.0

This can be verified with the command 

```
docker images
```

## How to run

To run the solution and keep the log on screen, please run the following command

```
docker-compose up
```

If you want to detach the execution from the screen, please add the -d parameter

```
docker-compose up -d
```

## Test

### Curl

In order to test you can execute the following curl command

```
curl --location --request POST 'localhost:8080/api/v1/checkout/orders' \
--header 'Content-Type: application/json' \
--data-raw '{
    "clientId": 638,
    "date": "2020-07-05T06:45:17.130Z",
    "address": "804 Aliyah Rest",
    "products": [
        {
            "id": 843,
            "quantity": 99,
            "cost": 178.86
        },
        {
            "id": 98,
            "quantity": 997,
            "cost": 740.21
        },
        {
            "id": 269,
            "quantity": 507,
            "cost": 199.92
        }
    ]
}'
```

### Postman

A Postman collection file `checkout.postman_collection.json` can be imported in Postman with an example to test the Checkout API

## Swagger

When the Checkout application is running the auto-generated swagger document can be seen in this URL

[http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html "Swagger Doc")

## Code Analysis

Results of code analysis by Sonar can be found in the following link

[https://sonarcloud.io/dashboard?id=jorgemil_ecommerce](https://sonarcloud.io/dashboard?id=jorgemil_ecommerce "Sonarcloud Analysis")

## TODO list

- Add Junit tests.
- Add persistence to store the status of the orders.
- Add validation using Bean Validation spec.
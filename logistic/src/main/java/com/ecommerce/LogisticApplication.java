package com.ecommerce;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@RabbitListener(queues = "ecommerce.logistic")
@ImportResource("classpath:/service-integration-config.xml")
public class LogisticApplication {

	@Bean
	public Queue logisticQueue() {
		return new Queue("ecommerce.logistic");
	}

	public static void main(String[] args) {
		SpringApplication.run(LogisticApplication.class, args);
	}

}

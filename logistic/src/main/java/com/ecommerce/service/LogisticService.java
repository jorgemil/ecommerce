package com.ecommerce.service;

import java.time.OffsetDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.ecommerce.domain.Order;

@Service
public class LogisticService {
	
	private static final Logger LOG = LoggerFactory.getLogger(LogisticService.class);

	@ServiceActivator
	public void sendOrder(
			@Header("dateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime dateTime,
			@Payload Order order) {

		LOG.info("Order {} Received to Send to {}", order.getOrderId(), order.getAddress());

	}
}
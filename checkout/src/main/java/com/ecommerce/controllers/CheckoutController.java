package com.ecommerce.controllers;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.domain.Order;

import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("api/v1/checkout")
public class CheckoutController {
	
	private static final Logger LOG = LoggerFactory.getLogger(CheckoutController.class);
	
	private final MessageChannel billingChannel;
	private final MessageChannel logisticChannel;
	
	public CheckoutController(@Qualifier("billingRequest") MessageChannel billingChannel,
			@Qualifier("logisticRequest") MessageChannel logisticChannel) {
		this.billingChannel = billingChannel;
		this.logisticChannel = logisticChannel;
	}

	@PostMapping("orders")
	@ApiResponse(responseCode = "201")
	public ResponseEntity<Order> processOrder(@RequestBody final Order order) {
		
		LOG.info("Order received. Client id: {}", order.getClientId());
		
		order.setOrderId(UUID.randomUUID().toString());
		

        Message<Order> message = MessageBuilder.withPayload(order)
                .setHeader("dateTime", OffsetDateTime.now())
                .build();

        billingChannel.send(message);
        LOG.debug("Message sent to billing request channel with id: {}", order.getOrderId());
		
        logisticChannel.send(message);
        LOG.debug("Message sent to logistic request channel with id: {}", order.getOrderId());
        
		return new ResponseEntity<>(order, HttpStatus.CREATED);
	}
}

package com.ecommerce.service;

import java.time.OffsetDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.ecommerce.domain.Order;

@Service
public class BillingService {
	
	private static final Logger LOG = LoggerFactory.getLogger(BillingService.class);

	@ServiceActivator
	public void generateBill(
			@Header("dateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime dateTime,
			@Payload Order order) {

		LOG.info("Order Received: {}", order.getOrderId());
		
		Double totalCost = order.getProducts()
				.stream()
				.map(product -> product.getCost() * product.getQuantity())
				.reduce(0d, Double::sum);
		
		String displayTotalCost = String.format("%.2f", totalCost);
		
		LOG.info("Total cost: {}", displayTotalCost);
	}
}

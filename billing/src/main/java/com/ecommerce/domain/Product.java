package com.ecommerce.domain;

import java.io.Serializable;

public class Product implements Serializable {

	private static final long serialVersionUID = 7887249030299082567L;

	private Long id;
	private Long quantity;
	private Double cost;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", quantity=" + quantity + ", cost=" + cost + "]";
	}
}

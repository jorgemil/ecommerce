package com.ecommerce;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@RabbitListener(queues = "ecommerce.billing")
@ImportResource("classpath:/service-integration-config.xml")
public class BillingApplication {

	@Bean
	public Queue billingQueue() {
		return new Queue("ecommerce.billing");
	}

	public static void main(String[] args) {
		SpringApplication.run(BillingApplication.class, args);
	}

}
